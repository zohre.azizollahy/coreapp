﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asp.netCore.ViewModels
{
    public class DepartmentViewModel
    {
        [Required]
        [Display(Name = " نام شرکت ")]
        public string Name { get; set; }
    }
}
