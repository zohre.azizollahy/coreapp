﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asp.netCore.ViewModels
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        [Required]
        [Display(Name = " نام و نام خانوادگی ")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "حقوق دریافتی ")]
        public decimal Salary { get; set; }
        public int? BossId { get; set; }
        public EmployeeViewModel Boss { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentViewModel Department { get; set; }
    }
}
