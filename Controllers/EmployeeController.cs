﻿using Asp.netCore.Models;
using Asp.netCore.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asp.netCore.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _service;
        public EmployeeController(IEmployeeService service)
        {
            _service = service;
        }
        public async Task<ActionResult<IEnumerable<Employee>>> Index()
        {
            var employees = await _service.GetAllEmploeeysAsync();
            return View(employees);
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();
            Employee employee = await _service.GetEmployeeAsync(id);
            if (employee == null) return NotFound();
            return View(employee);
        }
        public IActionResult Create()
        {
            ViewBag.Bosses = _service.GetDropDownEmployees();
            ViewBag.Departments = _service.GetDropDownDepartments();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                await _service.AddAsync(employee);
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }
        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Employee employee = await _service.GetEmployeeAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            ViewBag.Bosses = _service.GetDropDownEmployees();
            ViewBag.Departments = _service.GetDropDownDepartments();
            return View(employee);
        }
        // POST: Employees/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Employee employee)
        {
            if (id != employee.EmployeeId)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                await _service.UpdateAsync(employee);
                return View(employee);
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Employee employee = await _service.GetEmployeeAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }
        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.DeleteConfirmedAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
