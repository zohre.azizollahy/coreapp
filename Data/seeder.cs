﻿using Asp.netCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asp.netCore.Data
{
    public class Seeder
    {
        private readonly MicrodevDbContext _context;
        public Seeder(MicrodevDbContext context)
        {
            _context = context;
        }
        public async Task Seed()
        {
            _context.Database.EnsureCreated();
            if (!_context.Departments.Any())
            {
                List<Department> departments = new List<Department>
                {
                new Department{Name="مدیریت روشمند "},
                new Department{Name="دکان باز "},
                new Department{Name="ایده پرداز "},
                };
                _context.Departments.AddRange(departments);
            }
            if (!_context.Employees.Any())
            {
                List<Employee> employees = new List<Employee>
                {
                new Employee {Name="زهرا بیات ", Salary=1000 ,BossId=3,
                DepartmentId=1 },
                new Employee {Name="علی بیات ", Salary=3000 ,BossId=5,
                DepartmentId=2 },
                new Employee {Name="امین مژگانی ", Salary=1000, BossId=4,
                DepartmentId=1 },
                new Employee {Name="محمد سهیلی ", Salary = 4000,BossId = 1, DepartmentId = 1 },
                new Employee {Name="سارا احمدی ", Salary=1000, BossId=4, DepartmentId=2 },
                new Employee {Name="محمد سعیدی ", Salary=1000, BossId=4, DepartmentId=2 },
                new Employee {Name="سهیلا افتخاری ", Salary=2000, BossId=4, DepartmentId=2 },
                new Employee {Name="سعید حسینی ", Salary=5000, BossId=4, DepartmentId=3},
                new Employee {Name="زهرا محمدی ", Salary=1000, BossId=8, DepartmentId=3 }
                };
                _context.Employees.AddRange(employees);
            }
            await _context.SaveChangesAsync();
        }
    }
}
