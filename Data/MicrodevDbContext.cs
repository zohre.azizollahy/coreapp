﻿using Asp.netCore.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asp.netCore.Data
{
    public class MicrodevDbContext:DbContext
    {
        public MicrodevDbContext(DbContextOptions<MicrodevDbContext> options) : base(options) { }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
    }
}
